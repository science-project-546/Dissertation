# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# Привет, Дима!
# 
# Так как мы с тобой сейчас уже пишем готовый инструмент для научного анализа, нужно сопроводить следующее задание некой преамбулой.
# 
# Далее говорим только про модель Изинга.
# 
# Вспомним метод Монте-Карло (МК). В нём мы имеем одну конфигурацию спинов в качестве текущей. Для перехода в другую мы сначала строим пробную конфигурацию путём элементарного изменения проекции одного из спинов. Если энергия пробной конфигурации оказалась меньше текущей (dE < 0), то пробная конфигурация принимается гарантированно. Если же наоборот (dE > 0), то пробная конфигурация принимается с вероятностью P = exp( -dE / T ).
# 
# При некотором фиксированном T, действительная реализация итеративного процесса всегда состоит из двух фаз. В первой алгоритм движется в сторону той части Гильбертова пространства, где он в состоянии квази-равновесно существовать - энергия при этом активно уменьшается. Во второй фазе алгоритм просто существует в этой части - энергия при этом блуждает вокруг некого характерного для этой части значения.
# 
# Также стоит отдельно обговорить, какую температуру мы называем высокой, умеренной и низкой. Формально говоря, т.е. так, как это обычно описывают в статьях о физическом моделировании, высокая температура - это такое T, которое в 2-3 раза больше, чем самый большой энергетический параметр гамильтониана (взятый по модулю, естественно). Это (опять же, формально) означает, что мы в данном случае температурой размываем значимость каких-то определенных физических механизмов, заложенных в гамильтониане: при высоком T любая тенденция имеет возможность реализации. Умеренная температура - это T, примерно совпадающая по величине с минимальным энергетическим параметром гамильтониана (по модулю). Низкая температура - Т в разы меньше минимального энергетического параметра.
# 
# Чисто технически, можно описать несколько более просто и конкретно. Для каждого конкретного гамильтониана переворот одного спина будет означать какое-то dE. И если мы грубо оценим характерное <dE> для всего гамильтониана (на какую величину, в среднем, отличаются по энергии текущая и пробная конфигурации), то высокая температура - <dE> / T << 1; умеренная - <dE> /  T ~ 1; низкая - <dE> / T << 1.
# 
# Теперь к сути. Обычным подходом для нахождения основного состояния системы, или ввода системы в какой-то температурный режим (в котором усреднение какой-то величины будет физический оправданным и соответствовать реальной зависимости этой величины от температуры), является метод адиабатического охлаждения от высокой температуры до требуемой (или низкой). То есть мы стартуем симуляцию с высокой температуры, даем ей "время" уйти во вторую фазу, затем уменьшаем температуру на небольшую величину, вновь даём время уйти во вторую фазу - и так далее, пока не спустимся до нужной температуры.
# 
# Уязвимость такого подхода заключается в том, что при высокой температуре (P практически равно 1) мы имеем практически случайное блуждание по Гильбертову пространству - НО! Но - вокруг точки старта симуляции, а не равномерно по всему пространству. Поэтому остается паразитная зависимость от точки старта, и ограниченность охвата пространства неким радиусом вокруг этой точки.
# 
# Для решения этой проблемы мы и применяем PEDD. Она обеспечивает нам равномерное блуждание. То, что ты сейчас реализовал - идеальное блуждание системы при T = +inf. Теперь надо объединить эти два подхода. Чтобы мы не просто бегали по пространству, но и имели приоритет вхождения в конфигурации с меньшей энергией. Для этого предлагаю сделать так:
# 
# 1) Предполагается, что вся симуляция происходит за N итераций.
# 2) Вводим дополнительный вещественный параметр Alpha от 0 до 1.
# 3) Каждая итерация начинается с того, что мы должны выбрать механизм обработки текущей конфигурации: PEDD или МК. Генерируем случайное число от 0 до 1. Если оно оказалось меньше Alpha, то PEDD, иначе - МК.
# 4) Если PEDD, то реализуем переход к новой конфигурации по схеме PEDD.
# 5) Если МК, то - в традиционном формате случайной очереди, которую нужно генерировать заново, как только она заканчивается - выбираем один спин, элементарно меняем его проекцию, и принимаем (или не принимаем) конфигурацию по логике МК. Единственное дополнение - принятая (или не принятая) конфигурация должна быть отражена на текущем распределении M для PEDD.
# 
# Базовый тест видится таким:
# 1) Берем сложную систему множества спинов с большим количеством проекций.
# 2) Много раз запускаем стандартный вариант МК, реализуя метод адиабатического охлаждения до условного нуля. Среди всех этих запусков находим те, которые достигли самой низкоэнергетической конфигурации. Много ли таких?
# 3) Много раз запускаем вариант МК+PEDD в следующей формации. Стартуем симуляцию с умеренной температуры и Alpha = 1 (только PEDD). Адиабатически снижаем Alpha до нуля (или до какого-то малого значения - нужно пробовать). Затем адиабатически снижаем температуру до условного нуля. Получилось ли получить такую же самую низкоэнергетическую конфигурацию, как "чистый" МК (ну или не такую же, но с той же энергией - мало ли вырождена)? Много ли раз получилось?
# 4) Главный критерий - соотношение этих количеств запусков, в результате которых было успешно найдено основное состояние. 

# %%
import math
import numpy as np
import random
import matplotlib.pyplot as plt
import os
import time
import itertools
import time
from threading import Thread
from multiprocessing import Process, Manager, Lock


# %%
class State:
    def __init__(self, maxN, configSpin, indexToLengthSpin, maxStep):
        # Кол-во спинов
        self.maxN = maxN;

        # Магнитное поле
        self.J1 = 1;
        self.J2 = -0.5;
        # Взаимодействие
        self.H = 0.1 * self.J1;

        # Больцман
        self.k = 1.38 * 1e-23;       
        
        # Массив с индексами
        # Индексы для второго массива который их в длинну спина конвертирует
        self.beginConfigSpin = configSpin.copy();
        self.configSpin = configSpin;
        self.indexToLengthSpin = indexToLengthSpin;

        self.maxT = 1000;
        self.currentT = 1000;

        self.minE = float("inf");       


    def getE(self):
        return self.getE1(self.configSpin);

    def getE1(self, configSpin):
        #E = -h * sum(Si) - J1 * sum(Si * Sj) (соседи) - J2 * sum(Si * Sk) (спин через одного)
        sumFirst = 0;
        sumSecond = 0;
        sumThird = 0;
        for i in range(configSpin.size):
            j = i + 1 if i != configSpin.size - 1 else 0;
            #Ошибка 0 или 1 доложно быть в конечном условии
            k = i + 2 if i < configSpin.size - 2 else 0 if i != configSpin.size - 1 else 1;          

            S1 = self.indexToLengthSpin[int(configSpin[i])];
            S2 = self.indexToLengthSpin[int(configSpin[j])];
            S3 = self.indexToLengthSpin[int(configSpin[k])];

            sumFirst += S1;
            sumSecond += S1 * S2;
            sumThird += S1 * S3;

        return - self.H * sumFirst - self.J1 / 2 * sumSecond - self.J2 / 2 * sumThird;

    def getM(self):
        return self.getM1(self.configSpin);

    def getM1(self, configSpin = None):
        result = 0;
        for i in configSpin:
            result += self.indexToLengthSpin[int(i)];
        return self.H * result;

    #Заменяет значения намагничености на индекс массива
    def convertMToIndex(self, m):
        maxLenghtSpin = math.fabs(self.indexToLengthSpin[0]);    
        return int(m + maxLenghtSpin * self.maxN);

    #Заменяет значения индекс на намагниченость
    def convertIndexToM(self, i):
        maxLenghtSpin = math.fabs(self.indexToLengthSpin[0]);        
        return int(i - maxLenghtSpin * self.maxN);


# %%
def gaussianFunction(x, maxN):
    mu = 0;
    sig = maxN;
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.))) + 1;

def Cnk(x, maxN):
    Spin0 = (int)(maxN / 2 - x);
    Spin1 = maxN - Spin0;
    return math.factorial(Spin0 + Spin1) / (math.factorial(Spin0) * math.factorial(Spin1));

def generateSample(state):  
    maxLenghtSpin = math.fabs(state.indexToLengthSpin[0]);
    size = int(2 * maxLenghtSpin * state.maxN + 1);
    state.sample = np.zeros(size);

    for i in range(size):
        m = state.convertIndexToM(i);
        state.sample[i] = Cnk(m, state.maxN);

    state.sampleSum = state.sample.sum();
    return;

def generateField(state):
    maxLenghtSpin = math.fabs(state.indexToLengthSpin[0]);
    size = int(2 * maxLenghtSpin * state.maxN + 1);
    state.field = np.zeros(size);
    fieldIndex = state.convertMToIndex(state.getM());
    state.field[fieldIndex] += 1;
    state.fieldSum = state.field.sum();
    return;

def getState(maxStep):
    maxN = 100;
    
    a = 0;
    indexToLengthSpin = np.array([i - 0.5 for i in range(-a, a + 2)]);    
    configSpin = np.random.randint(indexToLengthSpin.size, size=maxN);

    state = State(maxN, configSpin, indexToLengthSpin, maxStep);

    # Семпл как и Филд есть словарь где ключ-значение представлены
    # как M намагниченость - ключ, а значение - сколько раз алгоритм
    # получал такую намагниченость
    generateSample(state);
    generateField(state);
    
    return state;


# %%
def draw(sample, field, i, path):
    fig = plt.figure(figsize=(20,10));    
    fig.text(0.5, 0.1, 'M', ha='center', va='center');
    fig.text(0.1, 0.5, 'N', ha='center', va='center', rotation='vertical');

    ax_1 = fig.add_subplot(2, 2, 1);
    ax_2 = fig.add_subplot(2, 2, 2);
    ax_3 = fig.add_subplot(2, 2, 3);
    ax_4 = fig.add_subplot(2, 2, 4);
    ax_1.set_title("Sample");
    ax_2.set_title("Field");
    ax_3.set_title("Normal sample");
    ax_4.set_title("Normal field");
    
    x = np.array(range((int)(-sample.size / 2), (int)(sample.size / 2) + 1, 1));   

    ax_1.plot(x, sample);
    ax_2.plot(x, field);
    
    sampleSum = sample.sum();
    sample = sample * (1 / sampleSum) * 100;

    fieldSum = field.sum();
    field = field * (1 / fieldSum) * 100; 

    ax_3.plot(x, sample);
    ax_4.plot(x, field);

    fig.savefig(path + "/plot_" + str(i) + ".png");
    plt.close();
    return;

def getTextConfig(state, isBegin = False):
    result = "(";
    
    if isBegin == False:
        for val in state.configSpin:
            result += str(state.indexToLengthSpin[int(val)]) + ";";
    else:
        for val in state.beginConfigSpin:
            result += str(state.indexToLengthSpin[int(val)]) + ";";

    result += ")"
    return result;

def saveResult(states, count, timeSpent, maxStep):
    currentTime = time.strftime("%H_%M_%S_%d_%m_%Y", time.localtime());
    
    if os.path.exists(os.getcwd() + "/Result/") != True:
        os.mkdir(os.getcwd() + "/Result/");
        
    path = os.getcwd() + "/Result/" + currentTime;
    os.mkdir(path);

    experimentFile = open(path + "/experiment.txt", "a");

    minE = float("inf");
    meanMinE = 0;
    for state in states:
        if state.minE < minE:
            minE = state.minE;
        meanMinE += state.minE;

    countSucces = 0;
    for state in states:
        beginConfig = getTextConfig(state, True);
        endConfig = getTextConfig(state);
        E = state.minE;
        Text = '\n' + "Begin Configuration: " + beginConfig + '\n' \
            + "End Configuration: " + endConfig + '\n' \
            + "Start T: " + str(state.maxT) + '\n' \
            + "End T: " + str(state.currentT) + '\n' \
            + "Min E configuration: " + str(E);

        if hasattr(state, 'alpha'):
            Text += '\n' + "Alpha: " + str(state.alpha) + '\n';
        else:
            Text += '\n';


        experimentFile.write(Text);

        if E == minE:
            countSucces += 1;
    
    meanMinE /= states.size;

    Text = '\n' + "Time spent: " + str(timeSpent)  + '\n' \
        + "Max spin: " + str(math.fabs(state.indexToLengthSpin[0])) + '\n' \
        + "Min E: " + str(minE) + '\n' \
        + "Max step: " + str(maxStep)  + '\n' \
        + "Equipartition min E: " + str(meanMinE)  + '\n' \
        + "Count experements: " + str(count) + '\n' \
        + "Count succes: " + str(countSucces / count * 100) + "%" + '\n';
    experimentFile.write(Text);

    figPath = path + "/Figs";
    os.mkdir(figPath);
    for i, state in enumerate(states):
        draw(states[0].sample, states[0].field, i, figPath);

    return;


# %%
def pedd(state, configs, leftRight):

    random.shuffle(configs);
    random.shuffle(leftRight);

    newConfigSpin = state.configSpin.copy();
    currentH = float("inf");

    for config in configs:
        for direction in leftRight:
            newlenght = direction + newConfigSpin[config];
            if newlenght < 0 or newlenght >= state.indexToLengthSpin.size:
                continue;
                
            tempConfigSpin = state.configSpin.copy();
            tempConfigSpin[config] = newlenght;
            
            m = state.getM1(tempConfigSpin);   
            fieldIndex = state.convertMToIndex(m);
                
            h1 = state.sample[fieldIndex] - state.sampleSum / (state.fieldSum + 1) * (state.field[fieldIndex] + 1);
            if h1 < 0 :
                h1 = 0;

            h2 = state.sample[fieldIndex] - state.sampleSum / state.fieldSum * (state.field[fieldIndex]);
            if h2 < 0 :
                h2 = 0;

            h = h1 - h2;

            if h < currentH:
                currentH = h;
                newConfigSpin = tempConfigSpin.copy();
            
    state.configSpin = newConfigSpin;
    fieldIndex = state.convertMToIndex(state.getM());
    state.field[fieldIndex] += 1;
    state.fieldSum += 1;
    return;

def mk(state):
    
    index = random.randint(0, state.maxN - 1);
    newConfigSpin = state.configSpin.copy();

    if state.configSpin[index] + 1 == state.indexToLengthSpin.size:
        newConfigSpin[index] -= 1;
    elif state.configSpin[index] - 1 < 0:
        newConfigSpin[index] += 1;
    else:
        rand = random.random();
        if rand > 0.5:
            newConfigSpin[index] += 1;
        else:
            newConfigSpin[index] -= 1;  

    oldEnergy = state.getE();
    newEnergy = state.getE1(newConfigSpin);
    delta = newEnergy - oldEnergy;

    # Принимаем или нет новую конфигурацию
    if delta <= 0:
        state.configSpin = newConfigSpin;
    else:
        chance = math.exp(-delta / (state.k * state.currentT));
        rand = random.random();
        if chance >= rand:
            state.configSpin = newConfigSpin;

    # Учитываем новую конфигурацию
    fieldIndex = state.convertMToIndex(state.getM1(newConfigSpin));
    state.field[fieldIndex] += 1;
    state.fieldSum += 1;    

    return;

def experimentWithoutAlpha(maxStep, states):
    state = getState(maxStep);      

    # Счётчик определящий когда надо понизить температуру
    maxStepBeforeTemperatureDrop = int(0.01 * maxStep);
    count = 0;  

    for i in range(maxStep):
        mk(state);

        currentE = state.getE();
        if currentE < state.minE:
            state.minE = currentE;

        # Понижаем температуру
        count += 1;
        if count == maxStepBeforeTemperatureDrop:
            state.currentT -= 0.05 * state.currentT;
            state.count = 0;   
            
    states.append(state);

def experiment(maxStep, states):
    state = getState(maxStep);
    configs = [ i for i in range(state.maxN)];
    leftRight = [ -1, 1];
    alpha = 1;

    # Счётчик определящий когда надо понизить альфу и температуру, а также когда будет понижаться альфа, а когда температура
    maxStepBeforeAlphaDrop = int(0.01 * maxStep);
    maxStepBeforeTemperatureDrop = int(0.01 * maxStep);
    maxStepAlphaDroped = maxStep * 0.5;
    count = 0; 
    
    for i in range(maxStep):
        beta = random.uniform(0, 1);
        if alpha > beta:
            pedd(state, configs, leftRight);
        else:
            mk(state);

        currentE = state.getE();
        if currentE < state.minE:
            state.minE = currentE;

        if i < maxStepAlphaDroped:
            count += 1;
            if count == maxStepBeforeAlphaDrop:
                alpha -= 0.05 * alpha;
                count = 0;
        else:
            count += 1;
            if count == maxStepBeforeTemperatureDrop:
                state.currentT -= 0.05 * state.currentT;
                count = 0;     

    state.alpha = alpha;        
    states.append(state);


# %%
if __name__ == '__main__':

    # Сколько раз итерируем 
    maxStep = int(1e2);

    # Кол-во эксперементов
    count = 100;

    # Оконченые состояния
    manager = Manager();
    states = manager.list();

    startTime = time.time();

    jobs = [];

    for i in range(count):
        p = Process(target=experiment, args=(maxStep, states))
        jobs.append(p);

    for i, job in enumerate(jobs):                
        job.start();
        if i % 5 == 0:
            job.join();
            print("Finish:" + str(i));

    for job in jobs:
        job.join();

    # Времени потрачено
    timeSpent = time.time() - startTime;

    saveResult(np.array(states), count, timeSpent, maxStep);
